public class GildedRose {
    var items:[Item]
    
    public init(items:[Item]) {
        self.items = items
    }
    
    public func updateQuality() {
      items.map { (item) -> Category? in
            GoodsCategories.buildCategory(item: item)
      }.forEach { (category) in
        if category != nil {
            category?.update()
        }else{
            print("Category Not Found")
        }
      }
    }
}
