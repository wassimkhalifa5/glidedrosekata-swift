//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/2/20.
//

import Foundation

protocol Category {
    var item: Item { get set }
    
    func reduceQuality(value:Int?)
    func addQuality(value:Int?)
    func isExpired() -> Bool
    func setCurrentQuality()
    func setCurrentSellin()
    func setQualityAfterExpiration()
    func update()
    
}

extension Category{
    func setCurrentQuality(){
        reduceQuality()
    }
    func setQualityAfterExpiration() {
        reduceQuality()
    }
    func setCurrentSellin() {
        item.sellIn -= 1
    }
    func reduceQuality(value:Int?=1){
        if item.quality > 0 {
            item.quality -= value ?? 1
        }
    }
    func addQuality(value:Int?=1){
        if item.quality < 50 {
            item.quality += value ?? 1
        }
    }
    func  isExpired() -> Bool {
        return  self.item.sellIn < 0
    }
    
    func update() {
        setCurrentQuality()
        setCurrentSellin()
        if isExpired(){
            setQualityAfterExpiration()
        }
    }
}
