//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

struct ConjuredGoods: Category{
    var item: Item
    init(item: Item) {
        self.item = item
    }
    func setCurrentQuality() {
        reduceQuality(value: 2)
    }
    func setQualityAfterExpiration() {
        reduceQuality(value: 2)
    }
}
