//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

struct BackStagePassGoods :Category {
    var item: Item
    
    init(item: Item) {
      self.item = item
    }
    
    func setCurrentQuality() {
        addQuality()
        
        if item.sellIn <= 10 {
            if item.sellIn <= 5{
                addQuality()
            }
            addQuality()
        }
    }
    func setQualityAfterExpiration() {
        self.item.quality = 0
    }
}
