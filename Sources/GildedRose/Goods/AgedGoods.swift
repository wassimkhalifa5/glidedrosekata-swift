//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

struct AgedGoods :Category{
    var item: Item
    init(item: Item) {
          self.item = item
    }
    
    func setCurrentQuality() {
        addQuality()
    }
    func setQualityAfterExpiration() {
        addQuality()
    }
}
