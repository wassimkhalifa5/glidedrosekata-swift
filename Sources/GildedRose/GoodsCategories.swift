//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/2/20.
//

import Foundation

enum GoodsCategories: String{
    case agedBrie = "Aged Brie"
    case backstagePasses = "Backstage passes to a TAFKAL80ETC concert"
    case sulfuras = "Sulfuras, Hand of Ragnaros"
    case dexterityVest = "+5 Dexterity Vest"
    case conjuredCake = "Conjured Mana Cake"
    case exilirMongoose = "Elixir of the Mongoose"
    case unknown
    
    static func buildCategory(item: Item) -> Category? {
           let goodCategory = GoodsCategories(rawValue: item.name) ?? .unknown
           switch goodCategory {
           case .agedBrie:
              return  AgedGoods(item: item)
           case .backstagePasses:
               return BackStagePassGoods(item:  item)
           case .sulfuras:
               return LegendaryGoods(item: item)
           case .dexterityVest,.exilirMongoose:
               return  OrdinaryGoods(item: item)
           case .conjuredCake:
               return ConjuredGoods(item: item)
           case .unknown:
               return nil
           }
    }
}
