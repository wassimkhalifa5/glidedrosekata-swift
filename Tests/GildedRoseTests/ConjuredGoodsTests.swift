//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

@testable import GildedRose
import XCTest


class ConjuredGoodsTests: XCTestCase {
    
    func testUpdateBeforeExpiration(){
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 11, quality: 9)
        let ongoingConjuredFoo =  ConjuredGoods(item: ongoingFoo)
        ongoingConjuredFoo.update()
        XCTAssertFalse(ongoingConjuredFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,10)
        XCTAssertEqual(ongoingFoo.quality,7)
    }
    func testUpdateAfterExpiration(){
        
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingConjuredFoo =  ConjuredGoods(item: ongoingFoo)
        for _ in 0...ongoingFoo.sellIn{
            print(ongoingFoo.quality)
            ongoingConjuredFoo.update()
        }
        XCTAssertTrue(ongoingConjuredFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,-1)
        XCTAssertEqual(ongoingFoo.quality,-1)
    }
}
