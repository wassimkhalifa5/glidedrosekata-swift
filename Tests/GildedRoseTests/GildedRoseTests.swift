@testable import GildedRose
import XCTest


//Legacy Code tests
class GildedRoseTests: XCTestCase {

    func testFoo() {
        let items = [Item(name: "foo", sellIn: 0, quality: 0)]
        let app = GildedRose(items: items);
        app.updateQuality();
        XCTAssertEqual("foo", app.items[0].name);
    }
    func testdDexterityVestAfterFiveDays(){
        let items = [Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20)]
        let app = GildedRose(items: items)
        let days = 5
        for _ in 0..<days {
            app.updateQuality()
        }
        let dexterityItem = app.items[0]
        XCTAssertEqual(dexterityItem.name, "+5 Dexterity Vest")
        XCTAssertEqual(dexterityItem.sellIn, 5)
        XCTAssertEqual(dexterityItem.quality, 15)
    }
    func testdAgedBrieAfterFiveDays(){
        let items = [Item(name: "Aged Brie", sellIn: 2, quality: 0)]
        let app = GildedRose(items: items)
        let days = 5
        for _ in 0..<days {
            app.updateQuality()
        }
        let agedBrie = app.items[0]
        XCTAssertEqual(agedBrie.name, "Aged Brie")
        XCTAssertEqual(agedBrie.sellIn, -3)
        XCTAssertEqual(agedBrie.quality, 8)
    }
    func testExlirOfMongooseAfterFiveDays(){
           let items = [Item(name: "Elixir of the Mongoose", sellIn: 5, quality: 7)]
           let app = GildedRose(items: items)
           let days = 5
           for _ in 0..<days {
               app.updateQuality()
           }
           let mongooseElixir = app.items[0]
           XCTAssertEqual(mongooseElixir.name, "Elixir of the Mongoose")
           XCTAssertEqual(mongooseElixir.sellIn, 0)
           XCTAssertEqual(mongooseElixir.quality, 2)
    }
    func testSulfurasAfterFiveDays(){
        let items = [Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80),
                     Item(name: "Sulfuras, Hand of Ragnaros", sellIn: -1, quality: 80)]
        
        let app = GildedRose(items: items)
        let days = 5
        for _ in 0..<days {
            app.updateQuality()
        }
       
        let sulfurasOne = app.items[0]
            XCTAssertEqual(sulfurasOne.name, "Sulfuras, Hand of Ragnaros")
            XCTAssertEqual(sulfurasOne.sellIn, 0)
            XCTAssertEqual(sulfurasOne.quality, 80)
        
        let sulfurasTwo = app.items[1]
            XCTAssertEqual(sulfurasTwo.name, "Sulfuras, Hand of Ragnaros")
            XCTAssertEqual(sulfurasTwo.sellIn, -1)
            XCTAssertEqual(sulfurasTwo.quality, 80)
    }
    func testBackStagePassesAfterFiveDays(){
        let items = [Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20),
                     Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 49),
                     Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 5, quality: 49)]
        
        let app = GildedRose(items: items)
        let days = 5
        for _ in 0..<days {
            app.updateQuality()
        }
       
       let backStageOne = app.items[0]
           XCTAssertEqual(backStageOne.name, "Backstage passes to a TAFKAL80ETC concert")
           XCTAssertEqual(backStageOne.sellIn, 10)
           XCTAssertEqual(backStageOne.quality, 25)
       
       let backStageTwo = app.items[1]
           XCTAssertEqual(backStageTwo.name, "Backstage passes to a TAFKAL80ETC concert")
           XCTAssertEqual(backStageTwo.sellIn, 5)
           XCTAssertEqual(backStageTwo.quality, 50)
        
       let backStageThree = app.items[2]
           XCTAssertEqual(backStageThree.name, "Backstage passes to a TAFKAL80ETC concert")
           XCTAssertEqual(backStageThree.sellIn, 0)
           XCTAssertEqual(backStageThree.quality, 50)
    }
    static var allTests : [(String, (GildedRoseTests) -> () throws -> Void)] {
        return [
            ("testFoo", testFoo),
        ]
    }
}
