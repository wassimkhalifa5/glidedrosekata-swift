//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

@testable import GildedRose
import XCTest

class AgedGoodsTests: XCTestCase {
    
    func testUpdate(){
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingAgedFoo =  AgedGoods(item: ongoingFoo)
        ongoingAgedFoo.update()
        XCTAssertFalse(ongoingAgedFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,9)
        XCTAssertEqual(ongoingFoo.quality,10)
    }
    func testUpdateAfterExpiration(){
        
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingAgedFoo =  AgedGoods(item: ongoingFoo)
        for _ in 0...ongoingFoo.sellIn{
            ongoingAgedFoo.update()
        }
        XCTAssertTrue(ongoingAgedFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,-1)
        XCTAssertEqual(ongoingFoo.quality,21)
    }
}
