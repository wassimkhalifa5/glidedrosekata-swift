//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation
@testable import GildedRose
import XCTest

class LegendaryGoodsTests: XCTestCase {
    
    func testUpdate(){
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingLengendaryFoo =  LegendaryGoods(item: ongoingFoo)
        ongoingLengendaryFoo.update()
        XCTAssertFalse(ongoingLengendaryFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,10)
        XCTAssertEqual(ongoingFoo.quality,9)
    }
}
