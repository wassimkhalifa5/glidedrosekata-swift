//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/2/20.
//
@testable import GildedRose
import XCTest

class GoodsCategoriesTests: XCTestCase {
    
    func testPredefinedTypes() {
        XCTAssertEqual(GoodsCategories.agedBrie.rawValue, "Aged Brie")
        XCTAssertEqual(GoodsCategories.backstagePasses.rawValue, "Backstage passes to a TAFKAL80ETC concert")
        XCTAssertEqual(GoodsCategories.sulfuras.rawValue, "Sulfuras, Hand of Ragnaros")
        XCTAssertEqual(GoodsCategories.dexterityVest.rawValue, "+5 Dexterity Vest")
        XCTAssertEqual(GoodsCategories.conjuredCake.rawValue, "Conjured Mana Cake")
        XCTAssertEqual(GoodsCategories.exilirMongoose.rawValue, "Elixir of the Mongoose")
    }
    func testCategoryBuilder(){
        let items = [
        Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20),
        Item(name: "Aged Brie", sellIn: 2, quality: 0),
        Item(name: "Elixir of the Mongoose", sellIn: 5, quality: 7),
        Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80),
        Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20),
        Item(name: "Conjured Mana Cake", sellIn: 3, quality: 6)]
        
        
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[0]) is OrdinaryGoods)
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[1]) is AgedGoods)
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[2]) is OrdinaryGoods)
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[3]) is LegendaryGoods)
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[4]) is BackStagePassGoods)
        XCTAssertTrue(GoodsCategories.buildCategory(item:items[5]) is ConjuredGoods)
    }
}
