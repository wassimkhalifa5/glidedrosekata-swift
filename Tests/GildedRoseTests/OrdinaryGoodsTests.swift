//
//  File.swift
//  
//
//  Created by Macbook Pro 2017 on 11/2/20.
//

import Foundation
@testable import GildedRose
import XCTest

class OrdinaryGoodsTests: XCTestCase {
    
    func testReduceQuality() {
        let fooItem  = Item(name: "Foo", sellIn: 0, quality: 9)
        let ordinaryGood =  OrdinaryGoods(item: fooItem)
        ordinaryGood.reduceQuality()
        XCTAssertEqual(fooItem.quality, 8)
        
        ordinaryGood.reduceQuality(value: 2)
        XCTAssertEqual(fooItem.quality, 6)
        
    }
    func testAddQuality() {
           let fooItem  = Item(name: "Foo", sellIn: 0, quality: 9)
           let ordinaryGood =  OrdinaryGoods(item: fooItem)
           ordinaryGood.addQuality()
           XCTAssertEqual(fooItem.quality, 10)
           
           ordinaryGood.addQuality(value: 2)
           XCTAssertEqual(fooItem.quality, 12)
           
       }
    func testIsExpired() {
        let expiredFoo  = Item(name: "expiredFoo", sellIn: -1, quality: 9)
        let expiredOrdinaryFoo =  OrdinaryGoods(item: expiredFoo)
        XCTAssertTrue(expiredOrdinaryFoo.isExpired())
        
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingOrdinaryFoo =  OrdinaryGoods(item: ongoingFoo)
        XCTAssertFalse(ongoingOrdinaryFoo.isExpired())
    }
    
    func testUpdate(){
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingOrdinaryFoo =  OrdinaryGoods(item: ongoingFoo)
        ongoingOrdinaryFoo.update()
        XCTAssertFalse(ongoingOrdinaryFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,9)
        XCTAssertEqual(ongoingFoo.quality,8)
    }
}
