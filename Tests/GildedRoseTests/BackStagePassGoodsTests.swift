//
//  File.swift
//
//
//  Created by Macbook Pro 2017 on 11/3/20.
//

import Foundation

@testable import GildedRose
import XCTest


class BackStagePassGoodsTests: XCTestCase {
    
    //sellin > 10
    func testUpdateSellInGreaterThenTen(){
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 11, quality: 9)
        let ongoingBackStageFoo =  BackStagePassGoods(item: ongoingFoo)
        ongoingBackStageFoo.update()
        XCTAssertFalse(ongoingBackStageFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,10)
        XCTAssertEqual(ongoingFoo.quality,10)
    }
    //sellin <= 10 && sellin > 5
    func testUpdateSellInLessThenTenGreaterThenFive(){
           let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 7, quality: 9)
           let ongoingBackStageFoo =  BackStagePassGoods(item: ongoingFoo)
           ongoingBackStageFoo.update()
           XCTAssertFalse(ongoingBackStageFoo.isExpired())
           XCTAssertEqual(ongoingFoo.sellIn,6)
           XCTAssertEqual(ongoingFoo.quality,11)
       }
    //sellin <= 5
    func testUpdateSellInLessThenTenLessThenFive(){
              let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 4, quality: 9)
              let ongoingBackStageFoo =  BackStagePassGoods(item: ongoingFoo)
              ongoingBackStageFoo.update()
              XCTAssertFalse(ongoingBackStageFoo.isExpired())
              XCTAssertEqual(ongoingFoo.sellIn,3)
              XCTAssertEqual(ongoingFoo.quality,12)
    }
    //sellin < 0
    func testUpdateAfterExpiration(){
        
        let ongoingFoo  = Item(name: "ongoingFoo", sellIn: 10, quality: 9)
        let ongoingBackStageFoo =  BackStagePassGoods(item: ongoingFoo)
        for _ in 0...ongoingFoo.sellIn{
            ongoingBackStageFoo.update()
        }
        XCTAssertTrue(ongoingBackStageFoo.isExpired())
        XCTAssertEqual(ongoingFoo.sellIn,-1)
        XCTAssertEqual(ongoingFoo.quality,0)
    }
}

