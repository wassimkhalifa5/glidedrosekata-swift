# GlidedRoseKata-Swift

**How to run it ?**

1. Clone the project
2. Open the folder with XCode 
3. Choose the GlidedRoseApp scheme 
4. Edit scheme -> Run -> Arguments -> Replace Arguments Passed on launch (Default 5)
5. Run the scheme by clicking on play 

**How to run Unit Tests ?**
1. Product -> Test (or) command + u 

 **Code Coverage = 93.5%**

 
 **Tasks Followed**

1. Task-1: Setup Repo 
2. Task-2: Add unit tests to validate legacy code 
3. Task-3: Create a predfined set of types using an enum and replace the hard coded strings
4. Task-4: Create a parent class that holds common logic and also create subclasses for each type 
5. Task-5: Add specific logic for each sub category
6. Task-6: Create a builder for the subcategories and replace the legacy code with the new objects 
7. Task-7: Remove the legacy code and replace it with the new update function
8. Task-8: Clean code and use prebuilt swift methods 